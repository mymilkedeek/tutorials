package net.mymilkedeek.groovytomcat

import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class GroovyHttpServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        req.session.setAttribute("language", "groovy")
        req.session.setAttribute("sentiment", "awesome")
        req.session.setAttribute("message", JavaGroovy.message())
        resp.sendRedirect("index.jsp")
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        super.doPost(req, resp)
    }
}