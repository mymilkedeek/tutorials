package net.mymilkedeek.groovytomcat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JavaServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getSession().setAttribute("language", "java");
        req.getSession().setAttribute("sentiment", "ok…");

        req.getSession().setAttribute("message", JavaGroovy.message());

        resp.sendRedirect("index.jsp"); }
}