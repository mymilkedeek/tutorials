package net.mymilkedeek.groovytomcat.project

import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * TODO: Write Documentation
 *
 * @author Michael Demey
 */
class GroovyCounter extends HttpServlet {

    @Override
    protected void doGet (HttpServletRequest req, HttpServletResponse resp) {
        def countObject = req.session.getAttribute("groovycounter")
        
        def count = 0
        
        if ( countObject != null ) {
            count = (Integer) countObject
        }
        
        req.session.setAttribute("groovycounter", count+1)
        
        resp.sendRedirect("counter.jsp")
    }
}
