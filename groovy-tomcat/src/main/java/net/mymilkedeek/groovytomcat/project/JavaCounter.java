package net.mymilkedeek.groovytomcat.project;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * TODO: Write Documentation
 *
 * @author Michael Demey
 */

public class JavaCounter extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Object countObject = req.getSession().getAttribute("javacounter");

        int count = 0;

        if ( countObject != null ) {
            count = (Integer) countObject;
        }

        req.getSession().setAttribute("javacounter", count+1);

        resp.sendRedirect("counter.jsp");
    }
}
