<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title></title>
</head>
<body>
    <p>Calls made:</p>
    <table>
        <tr>
            <td>Java</td>
            <td><c:out value="${javacounter}" /></td>
        </tr>
        <tr>
            <td>Groovy</td>
            <td><c:out value="${groovycounter}" /></td>
        </tr>
    </table>
</body>
</html>