<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Eek’s Groovy Sandbox</title>
</head>
<body>
<p>I’m using <c:out value="${language}" />! That’s <c:out value="${sentiment}" /></p>
<p><c:out value="${message}" /></p>
</body>
</html>